import React, { Component } from 'react';

// Components
import Header from './components/Header';
import Hero from './components/Hero';
import Main from './components/Main';
import Footer from './components/Footer';

import './assets/app.scss';

class App extends Component {
  render() {
    return [
      <Header key="HeaderComp" />,
      <Hero key="HeroComp" />,
      <Main key="MainComp" />,
      <Footer key="FooterComp" />
    ];
  }
}

export default App;
