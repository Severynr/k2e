import React from 'react';
import {Helmet} from "react-helmet";

class StyleGuidePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {title: 'Style guide'};
  }

  render() {
    const { state } = this;
    return [
      <Helmet
       title={`K2I - ${ state.title}`}
       key="Helmet"
       />,
      <div key="demo">
        <h1>Hello, this is { state.title } page</h1>
      </div>
    ]
  };
}

export default StyleGuidePage;
