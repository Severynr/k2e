import React from 'react';
import {Helmet} from "react-helmet";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {title: '404 Not Found'};
  }

  render() {
    const { state } = this;
    return [
      <Helmet
       title={`K2I - ${ state.title}`}
       key="Helmet"
       />,
      <div key="wrap">
        <h1>404 - page not found</h1>
      </div>
    ]
  };
}

export default HomePage;
