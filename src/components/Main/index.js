import React, { Component } from 'react';

import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

// Routes
import HomePage from '../../pages/HomePage';
import StyleGuidePage from '../../pages/StyleGuidePage';

class Main extends Component {
  render() {
  	return (
      <main className="k2i-main">
        <Switch key="SwicherForRouter">
          <Route exact path="/"  component={HomePage} />
          <Route path="/styleguide" component={StyleGuidePage} />
          <Redirect to="/" />
        </Switch>
      </main>
    )
  };
}

export default Main;