import React from 'react';
import { NavLink } from 'react-router-dom';

import '../../assets/styles/components/_header.scss';

const Header = () => (
	<header className="k2i-header">
      <div className="k2i-header__row k2i-header__row--top">
        <div className="k2i-logo k2i-header-logo">
            <NavLink
              exact
              activeClassName="active"
              to="/"
            >
            LOGO
            </NavLink>
        </div>
        <div className="k2i-header-contacts">
          <span>0118 324 3190 or 0800 690 6568</span>
          <span>info@key2investing.com</span>
        </div>
      </div>
      <div className="k2i-header__row k2i-header__row--bottom">
        <nav>
          <ul>
            <li>
              <a href="/" title="nav item text">INVEST YOUR MONEY</a>
            </li>
            <li>
              <a href="/" title="nav item text">HOW IT WORKS</a>
            </li>
            <li>
              <a href="/" title="nav item text">PROPERTY LOANS</a>
            </li>
            <li>
              <a href="/" title="nav item text">ABOUT</a>
            </li>
            <li>
              <a href="/" title="nav item text">BLOG</a>
            </li>
            <li>
              <a href="/" title="nav item text">CONTACT</a>
            </li>
            <li>
              <NavLink
                exact
                activeClassName="active"
                to="/styleguide"
              >
                SG
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
	</header>
);

export default Header;