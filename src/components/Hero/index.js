import React from 'react';

import '../../assets/styles/components/_header.scss';

const Hero = () => (
	<section className="k2i-hero">
		<div className="k2i-hero-inner">
			<div className="k2i-hero__content">
				<h5 className="k2i-hero__cta">Bringin</h5>
				<div className="k2i-hero__decoration" />
				<h2 className="k2i-hero__caption">PROPERTY INVESTMENT opportunities to Everyone</h2>
				<ul className="k2i-hero__features-list">
					<li className="k2i-feature-list__item">
						<span className="k2i-list-item__icon" />
						<div className="k2i-list-item__caption">
							Select your property investment
						</div>
					</li>
					<li className="k2i-feature-list__item">
						<span className="k2i-list-item__icon" />
						<div className="k2i-list-item__caption">
							Earn up to 26% p.a.
						</div>
					</li>
					<li className="k2i-feature-list__item">
						<span className="k2i-list-item__icon" />
						<div className="k2i-list-item__caption">
							Invest easily with automatic tools
						</div>
					</li>
					<li className="k2i-feature-list__item">
						<span className="k2i-list-item__icon" />
						<div className="k2i-list-item__caption">
							Projects professionally evaluated
						</div>
					</li>
				</ul>
			</div>
			<div className="k2i-hero__controls">
				<div className="k2i-controls__arrows">
					<span className="k2i-arrows__item-left">left</span>
					<span className="k2i-arrows__item-right">right</span>
				</div>
				<div className="k2i-hero-controls__bullets">
					bullets
				</div>
			</div>
		</div>
	</section>
);

export default Hero;